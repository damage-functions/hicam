library("rslurm")

sim.pi <- function(iterations = 1000) {
  # Generate two vectors for random points in unit 
  # circle
  x.pos <- runif(iterations, min=-1, max=1)
  y.pos <- runif(iterations, min=-1, max=1)
  
  # Test if draws are inside the unit circle
  draw.pos <- ifelse(x.pos^2 + y.pos^2 <= 1, TRUE, FALSE)
  draws.in <- length(which(draw.pos == TRUE))
  
  result <- data.frame(iterations,draws.in)
  
  return(result)
}

sim.pi()

params <- data.frame(iterations = rep(1000,100))

params

# sjob1 <- slurm_apply(
#   sim.pi, 
#   params,
#   jobname="rslurm-pi-example",
#   nodes = 2,
#   cpus_per_node = 2,
#   slurm_options = list(time = "0-1:00:00", mem-per-cpu = "2GB") )

sjob1 <- slurm_apply(
  sim.pi, 
  params,
  jobname="rslurm-pi-example",
  nodes=2,
  cpus_per_node=2,
  slurm_options = list(time = "1:00:00")
)

res <- get_slurm_out(sjob1, "table")
my_pi <- 4/(sum(res$iterations)/sum(res$draws.in))
cat("\n... done\n")
cat(
  paste0(
    "pi estimated to ", my_pi, " over ", sum(res$iterations), " iterations\n"
  )
)

