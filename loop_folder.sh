# --------------------------------------------------
#  File:        execute_script.sh
#
#  Created:     Mi 10-02-2021
#  Author:      Friedrich Boeing
#
# --------------------------------------------------
#
#  Description: this scripts applies the weighting program to calculate nuts-3 averages and loops over variables
#
#  Modified:
#
# --------------------------------------------------

 
set -e
set -x
do_cluster="False"

inpath="/data/hicam/data/processed/meteo/germany/station_eobs/mhm/"

for d in "/data/hicam/data/processed/meteo/germany/climproj/euro-cordex/88realizations_mhm/*/; do
	echo "$d"
done


