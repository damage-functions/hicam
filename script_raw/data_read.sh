#! /bin/bash
#-----------------------------------------------------
#$ -S /bin/bash            # Working Shell
#$ -N data_read     # Set job name
#$ -o /home/peichl/Proj3/jobs/$JOB_NAME.$JOB_ID # Log file
#$ -j y                    # Write error in log file
#$ -l h_rt=12:00:00         # Request resource: hard run time hours:minutes:seconds
#$ -l h_vmem=250G           # Request resource: memory requirement

#$ -binding linear:1

#$ -l highmem=true         # run on highmem nodes
# #$ -l centos6=true         # OS
# #$ -cwd                    # Change into directory where you wrote qsub
#$ -m ea                   # mail notification e - in case of job ended a - in case of abortion 
#$ -M michael.peichl@ufz.de # mail address
#----------------------------------------------------

module load R/3.4.3-1

export MC_CORES=${12}

Rscript ~/Proj3/DamageFunctions_Edge/script_raw/Klima_netcdf_to_sf.R

