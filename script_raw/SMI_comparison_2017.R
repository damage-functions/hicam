## Compare SMI data 2017 and 2017.

## Source namelist
source("namelist.R")


#### Load data ####
Meteo_historical_wide <- read_csv(paste(directory_processed, "historical/input_wide_2017.csv", sep=""))
# Data are derived from Meteo_netcdf_to_sf.R

#### Select only SMI
SMI <- Meteo_historical_wide %>% select(comId, contains("SMI"))

#################################################
#### Load shape of administrative districts ####
###############################################
print(krs) # loaded in namelist.R
print(comId)

#############################################
#### Function that loops through all SMI ####
plot_smi_fun <- function(x){
  #### Choose data for plotting
  data <- SMI_sf %>% select(USE:SHAPE_AREA, names_SMI[x], geometry)
  names(data) [6] <- "value"
  
  #### Plot SMI ####
  # list_crop[[1]]
  myPalette <- colorRampPalette((brewer.pal(10, "RdBu")))
  colours <- myPalette(10)
  # [c(1,3,7:11)]
  
  SMI_plot <-
    ggplot(data) +
    geom_sf(data=krs, fill="gray", color="white")  +
    # geom_sf(aes(fill = SMIL02.1951.01 )) + 
    geom_sf(aes(fill = cut(value, c(-0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0), 
                           labels = c(" < 0.1", " < 0.2", " < 0.3", " < 0.4", " < 0.5", " < 0.6", " < 0.7", " < 0.8", " < 0.9", " < 1")))) +
    theme(legend.text=element_text(size=20), legend.title = element_text(size=30) , plot.title = element_text(size=30)) +
    theme( axis.ticks.x = element_line(color="black"), axis.text.x = element_text(color="black" , size = 30)) +
    theme( axis.ticks.y = element_line(color="black"),  axis.text.y = element_text(color="black" , size = 30)) +
    scale_fill_manual(names_SMI[[x]], values= colours, 
                      limits=  c(" < 0.1", " < 0.2", " < 0.3", " < 0.4", " < 0.5", " < 0.6", " < 0.7", " < 0.8", " < 0.9", " < 1")) +
    theme(legend.position = "bottom") +
    # theme(legend.title=element_blank()) +
    theme(legend.key.height =   unit(0.5, 'cm')) +
    theme(legend.key.width = unit(2, "cm")) +
    guides(fill=guide_legend(ncol=10, nrow=1,  title.position="bottom", title.hjust = 0.5, byrow = TRUE, label.position="bottom"))
  SMI_plot
  
  ggsave(paste(directory_figures, "SMI/", "2017","_",names_SMI[[x]],".pdf", sep=""), width=12, height=16)
}
seq_along(names_SMI)

# plot_smi_fun(900)

## Apply function of all SMI columns
lapply(seq_along(names_SMI), plot_smi_fun)


