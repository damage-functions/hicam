## Generate Spatial Subunit for 


#######################
#### Load Data Set ####
crop=1
print("Crop used is:")
print(list_crop[crop])
YieldMeteo_load <- read_csv(paste(directory_processed, "YieldMeteo", "_", year, "_", list_crop[[crop]], ".csv", sep=""))
YieldMeteo_load %>% map(summary)

##
list_spatial_fs <- list("BB/MV/SA" = c(12,13,15), 
                        "SH/LS" = c(1,3), 
                        "NRW/HS" = c(5,6),
                        "SL/RP" =c(7,10), 
                        "BW/BY" = c(8,9), 
                        "TH/SN" = c(16,14), 
                        "SH" = 1, 
                        "LS" = 3, 
                        "NRW" = 5, 
                        "HS" = 6, 
                        "RP" = 7,  
                        "BW" = 8, 
                        "BY" = 9, 
                        "SL" = 10, 
                        "BB"= 12, 
                        "MV" = 13,  
                        "SN" = 14, 
                        "SA" = 15, 
                        "TH" = 16,
                        "allFS" = c(1, 3, 5, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16)) # spatial



## Schleswig-Holstein ##
YieldMeteo_load %>% select(stateId, state)%>% unique %>% filter(stateId == 1) 
YieldMeteo_SH <- YieldMeteo_load %>% filter(stateId==1)
YieldMeteo_SH
SHcomId <- data.frame(unique(YieldMeteo_SH$comId))
SHcom <- data.frame(unique(YieldMeteo_SH$com))
SH <- bind_cols(SHcomId, SHcom)
names(SH) <- c("comId", "com")
SH

SH_KS <- c(1001, 1002, 1003, 1004)
SH_NK <- SH %>% filter(!comId %in% c(SH_KS))  %>% select(comId) %>% list
SH_NK <- list(SH_NK[[1]]$comId )[[1]]

## FS ohne MG und KS
SH_noMGKS <- SH %>% filter(!comId %in% c(SH_KS))  %>% select(comId)
SH_noMGKS <- list(SH_noMGKS$comId)[[1]]


## Lower Saxony ##
YieldMeteo_load %>% select(stateId, state)%>% unique %>% filter(stateId == 3) 
YieldMeteo_LS <- YieldMeteo_load %>% filter(stateId == 3)
YieldMeteo_LS
LScomId <- data.frame(unique(YieldMeteo_LS$comId))
LScom <- data.frame(unique(YieldMeteo_LS$com))
LS <- bind_cols(LScomId, LScom)
names(LS) <- c("comId", "com")
LS

LS_HH <- c(3155, 3254, 3252, 3257, 3157, 3158)
LS_UL <- c(3152)
LS_WMG <- c(3255)
LS_MG <- c(3153)
LS_KS <- c(3101,3102, 3103, 3404)
LS_NK <- c(3461,3455 ,3462 , 3452 )

LS_LBN <- LS %>% filter(!comId %in% c(LS_HH, LS_UL, LS_WMG, LS_MG, LS_KS, LS_NK))  %>% select(comId)
LS_LBN <- list(LS_LBN$comId)[[1]]

## FS ohne MG und KS
LS_noMGKS <- LS %>% filter(!comId %in% c(LS_WMG, LS_MG, LS_KS))  %>% select(comId)
LS_noMGKS <- list(LS_noMGKS$comId)[[1]]

## North-Rhine-Westphalia ##
YieldMeteo_load %>% select(stateId, state)%>% unique %>% filter(stateId == 5) 
YieldMeteo_NRW <- YieldMeteo_load %>% filter(stateId == 5)
YieldMeteo_NRW
NRWcomId <- data.frame(unique(YieldMeteo_NRW$comId))
NRWcom <- data.frame(unique(YieldMeteo_NRW$com))
NRW <- bind_cols(NRWcomId, NRWcom)
names(NRW) <- c("comId", "com")
NRW

NRW_KS <- c(5111, 5112, 5116, 5119, 5315, 5316, 5911, 5913, 5915)
NRW_SWMG <- 5366
NRW_WMG <- c(5970,5966, 5374, 5962, 5958)
NRW_LBN <- c(5554, 5566, 5754, 5758, 5770)


NRW_RHA <- NRW %>% filter(!comId %in% c(NRW_KS, NRW_SWMG, NRW_WMG, NRW_LBN))  %>% select(comId) 
NRW_RHA <- list(NRW_RHA$comId)[[1]]

## FS ohne MG und KS
NRW_noMGKS <- NRW %>% filter(!comId %in% c(NRW_WMG, NRW_SWMG, NRW_KS))  %>% select(comId)
NRW_noMGKS <- list(NRW_noMGKS$comId)[[1]]

# Hessen 
YieldMeteo_load %>% select(stateId, state)%>% unique %>% filter(stateId == 6) 
YieldMeteo_HS <- YieldMeteo_load %>% filter(stateId == 6)
YieldMeteo_HS
HScomId <- data.frame(unique(YieldMeteo_HS$comId))
HScom <- data.frame(unique(YieldMeteo_HS$com))
HS <- bind_cols(HScomId, HScom)
names(HS) <- c("comId", "com")
HS 

HS_KS <- c(6411,6412 , 6413, 6414, 6611 )
HS_OR <- c(6436, 6413, 6432 , 6431, 6433)
HS_UL <- c(6435, 6631, 6632, 6535, 6636)
HS_WMG <- c(6532, 6635)

HS_RHA <- HS %>% filter(!comId %in% c(HS_OR, HS_UL, HS_WMG, HS_KS))  %>% select(comId) 
HS_RHA <- list(HS_RHA$comId)[[1]]

## FS ohne MG und KS
HS_noMGKS <- HS %>% filter(!comId %in% c(HS_WMG, HS_KS))  %>% select(comId)
HS_noMGKS <- list(HS_noMGKS$comId)[[1]]

#### Rheinland - Pfalz
YieldMeteo_load %>% select(stateId, state)%>% unique %>% filter(stateId == 7) 
YieldMeteo_RP <- YieldMeteo_load %>% filter(stateId == 7)
YieldMeteo_RP
RPcomId <- data.frame(unique(YieldMeteo_RP$comId))
RPcom <- data.frame(unique(YieldMeteo_RP$com))
RP <- bind_cols(RPcomId, RPcom)
names(RP) <- c("comId", "com")
RP 

RP_OR <- c(7231, 7339, 7133, 7331, 7333, 7338, 7334, 7337, 7332)
RP_SWMG <- RP %>% filter(!comId %in% RP_OR)  %>% select(comId) 
RP_SWMG <- list(RP_SWMG$comId)[[1]]

RP_KS <- c()

## FS ohne MG und KS
RP_noMGKS <- RP %>% filter(!comId %in% c(RP_SWMG, RP_KS))  %>% select(comId)
RP_noMGKS <- list(RP_noMGKS$comId)[[1]]

## Baden-Würtemmberg 
YieldMeteo_load %>% select(stateId, state)%>% unique %>% filter(stateId == 8) 
YieldMeteo_BW <- YieldMeteo_load %>% filter(stateId == 8)
YieldMeteo_BW
BWcomId <- data.frame(unique(YieldMeteo_BW$comId))
BWcom <- data.frame(unique(YieldMeteo_BW$com))
BW <- bind_cols(BWcomId, BWcom)
names(BW) <- c("comId", "com")
BW 

BW_KS <- c(8111)
BW_OR <- c(8226,8126, 8125, 8215, 8236, 8118, 8119, 8116, 8416, 8115, 8216, 8316, 8317)
BW_DI <-c(8436,8426, 8437 )
BW_UL <- c(8135, 8136, 8127, 8128, 8225)
BW_BS <- BW  %>% filter(!comId %in% c(BW_OR,BW_DI, BW_UL, BW_KS))  %>% select(comId) 
BW_BS <- list(BW_BS$comId)[[1]]

## FS ohne MG und KS
BW_noMGKS <- BW %>% filter(!comId %in% c(BW_BS, BW_KS))  %>% select(comId)
BW_noMGKS <- list(BW_noMGKS$comId)[[1]]


## Bayern
YieldMeteo_load %>% select(stateId, state)%>% unique %>% filter(stateId == 9) 
YieldMeteo_BY <- YieldMeteo_load %>% filter(stateId == 9)
YieldMeteo_BY
BYcomId <- data.frame(unique(YieldMeteo_BY$comId))
BYcom <- data.frame(unique(YieldMeteo_BY$com))
BY <- bind_cols(BYcomId, BYcom)
names(BY) <- c("comId", "com")
BY 
BY_KS <- c(9161,9162 , 9163, 9261, 9262, 9263, 9361, 9362, 9363, 9461,9462,9463,9464, 9561,9562,9563,9564,9565,9661,9662,9663,9761,9764)
BY_noKS <- BY  %>% filter(!comId %in% c(BY_KS))  %>% select(comId, com) 
BY_noKS  

BY_VA <- c(9777, 9181 , 9190 ,9173 , 9187, 9182   , 9183, 9189, 9171, 9172  ) #10
BY_DI <-c(9778, 9775 ,9774 ,9773  ,  9772, 9771 ,9185 ,9186  , 9179,9174 ,9188 , 9184,  9178, 9273, 9375,9274 , 
          9177, 9175 , 9278, 9271, 9279,9277 ,9275 ) #23
match(BY_DI,BY_VA)
BY_OB <- c(9476 ,9472 ,9477 , 9475 , 9479, 9377 ,9374  ,9372 , 9276 , 9376 ,9272 ) #11
BY_UL <- BY_noKS  %>% filter(!comId %in% c(BY_VA ,BY_DI, BY_OB))  %>% select(comId)  # 24
BY_UL <- list(BY_UL$comId)[[1]]

## FS ohne MG und KS
BY_noMGKS <- BY %>% filter(!comId %in% c(BY_VA, BY_KS))  %>% select(comId)
BY_noMGKS <- list(BY_noMGKS$comId)[[1]]

## Show Saarland comIds ##
YieldMeteo_load %>% select(stateId, state)%>% unique %>% filter(stateId == 10) 
YieldMeteo_SL <- YieldMeteo_load %>% filter(stateId == 10)
SLcomId <- data.frame(unique(YieldMeteo_SL$comId))
SLcom <- data.frame(unique(YieldMeteo_SL$com))
SL <- bind_cols(SLcomId, SLcom)
names(SL) <- c("comId", "com")
SL
SL_SWMG <- list(SL$comId)[[1]]

SL_KS <- c()

## FS ohne MG und KS
SL_noMGKS <- SL %>% filter(!comId %in% c(SL_SWMG, SL_KS))  %>% select(comId)
SL_noMGKS <- list(SL_noMGKS$comId)[[1]]
SL_noMGKS <- list()

## Brandenburg
YieldMeteo_load %>% select(stateId, state)%>% unique %>% filter(stateId == 12) 
YieldMeteo_BB <- YieldMeteo_load %>% filter(stateId == 12)
YieldMeteo_BB
BBcomId <- data.frame(unique(YieldMeteo_BB$comId))
BBcom <- data.frame(unique(YieldMeteo_BB$com))
BB <- bind_cols(BBcomId, BBcom)
names(BB) <- c("comId", "com")
BB

BB_NOBB <- c(12069, 12072, 12061, 12067, 12064, 12071, 12066, 12062)
BB_NOMV <- BB %>% filter(!comId %in% BB_NOBB)  %>% select(comId) 
BB_NOMV <- list(BB_NOMV$comId)[[1]]

BB_KS <- c()

## FS ohne MG und KS
BB_noMGKS <- BB %>% filter(!comId %in% c(BB_KS))  %>% select(comId)
BB_noMGKS <- list(BB_noMGKS$comId)[[1]]

## Mecklenburg-Vorpommern
YieldMeteo_load %>% select(stateId, state)%>% unique %>% filter(stateId == 13) 
YieldMeteo_MV <- YieldMeteo_load %>% filter(stateId == 13)
YieldMeteo_MV
MVcomId <- data.frame(unique(YieldMeteo_MV$comId))
MVcom <- data.frame(unique(YieldMeteo_MV$com))
MV <- bind_cols(MVcomId, MVcom)
names(MV) <- c("comId", "com")
MV

MV_NOBB <- c(13062, 13062)
MV_NK <- c(13061, 13058)

MV_NOMV <- MV %>% filter(!comId %in% c(MV_NOBB, MV_NK))  %>% select(comId) 
MV_NOMV <- list(MV_NOMV$comId)[[1]]

MV_KS <- c()

## FS ohne MG und KS
BB_noMGKS <- BB %>% filter(!comId %in% c(BB_KS))  %>% select(comId)
BB_noMGKS <- list(BB_noMGKS$comId)[[1]]

## Sachsen
YieldMeteo_load %>% select(stateId, state)%>% unique %>% filter(stateId == 14) 
YieldMeteo_SN <- YieldMeteo_load %>% filter(stateId ==14)
YieldMeteo_SN
SNcomId <- data.frame(unique(YieldMeteo_SN$comId))
SNcom <- data.frame(unique(YieldMeteo_SN$com))
SN <- bind_cols(SNcomId, SNcom)
names(SN) <- c("comId", "com")
SN

SN_KS <- c(14511, 14612, 14713 )
SN_NOBB <- c(14730, 14625)
SN_UL <- c(14628, 14523)
SN_MG <- c(14521)

SN_MO <- SN %>% filter(!comId %in% c(SN_NOBB, SN_UL, SN_MG, SN_KS))  %>% select(comId) 
SN_MO <- list(SN_MO$comId)[[1]]

## FS ohne MG und KS
SN_noMGKS <- SN %>% filter(!comId %in% c(SN_KS, SN_MG))  %>% select(comId)
SN_noMGKS <- list(SN_noMGKS$comId)[[1]]

## Sachsen - Anhalt
YieldMeteo_load %>% select(stateId, state)%>% unique %>% filter(stateId == 15) 
YieldMeteo_SA <- YieldMeteo_load %>% filter(stateId == 15)
YieldMeteo_SA
SAcomId <- data.frame(unique(YieldMeteo_SA$comId))
SAcom <- data.frame(unique(YieldMeteo_SA$com))
SA <- bind_cols(SAcomId, SAcom)
names(SA) <- c("comId", "com")
SA

SA_KS <- c(15003, 15001)
SA_NOBB <- c(15090, 15086, 15091, 15082)
SA_MG <- c(15085, 15087)

SA_MO <- SA %>% filter(!comId %in% c(SA_NOBB, SA_MG, SA_KS))  %>% select(comId) 
SA_MO <- list(SA_MO$comId)[[1]]

## FS ohne MG und KS
SA_noMGKS <- SA %>% filter(!comId %in% c(SA_KS, SA_MG))  %>% select(comId)
SA_noMGKS <- list(SA_noMGKS$comId)[[1]]

## Thüringen ##
YieldMeteo_load %>% select(stateId, state)%>% unique %>% filter(stateId == 16) 
YieldMeteo_TH <- YieldMeteo_load %>% filter(stateId == 16)
YieldMeteo_TH
THcomId <- data.frame(unique(YieldMeteo_TH$comId))
THcom <- data.frame(unique(YieldMeteo_TH$com))
TH <- bind_cols(THcomId, THcom)
names(TH) <- c("comId", "com")
TH

TH_KS <- c(16051, 16052, 16055, 16056)
TH_MG <- c(16072)
TH_MO <- c(16064  ,16064  ,16068  ,16071  , 16077)

TH_UL <- TH %>% filter(!comId %in% c(TH_MG, TH_MO, TH_KS))  %>% select(comId) 
TH_UL <- list(TH_UL$comId)[[1]]

## FS ohne MG und KS
TH_noMGKS <- TH %>% filter(!comId %in% c(TH_KS, TH_MG))  %>% select(comId)
TH_noMGKS <- list(TH_noMGKS$comId)[[1]]

###########################################################
#### Make list of non-KS comIDs for each Federal State ####
## List of federal states 
list_FS <- list("SH" = SH[[1]], "LS" = LS[[1]], "NRW" = NRW[[1]], "HS" = HS[[1]], "RP" = RP[[1]], "BW" = BW[[1]], "BY" = BY[[1]], 
                "SL" = SL[[1]], "BB" = BB[[1]], "MV" = MV[[1]], "SN" = SN[[1]], "SA" = SA[[1]], "TH" = TH[[1]])
names(list_FS)

## List of comIds of federal states only considering Kreisfreie Städte
list_FS_ks <- list("SH" = SH_KS, "LS" = LS_KS, "NRW" = NRW_KS, "HS" = HS_KS, "RP" = RP_KS, "BW" = BW_KS, "BY" = BY_KS, "SL" = SL_KS, "BB" = BB_KS, 
                   "MV" = MV_KS, "SN" = SN_KS, "SA" = SA_KS, "TH" = TH_KS)


## List of comIds of federal states without Kreisfreie Städte
FS_noKS_fun <- function(x){
list_FS_df <- as_data_frame(list_FS[[x]])
names(list_FS_df)<- "comId"
output <- list_FS_df %>% filter(!comId %in% c(list_FS_ks[[x]])) %>% select(comId)
output <- c(output[[1]])
output
}

list_FS_noks <- lapply(c(1:13), FS_noKS_fun)
# list_FS_noks[[13]]
FS_noKS_names <- c("SH_noKS", "LS_noKS", "NRW_noKS", "HS_noKS", "RP_noKS", "BW_noKS", "BY_noKS" , "SL_noKS" ,"BB_noKS", 
                   "MV_noKS" ,"SN_noKS" ,"SA_noKS", "TH_noKS")
names(list_FS_noks) <-  FS_noKS_names

#############################################
#### Combine BKR from FS to gerneral BKR ####


#### SüdwestMittelgebirge
SWMG <-list(c(RP_SWMG, SL_SWMG, NRW_SWMG))
names(SWMG) <- "SüdwestMittelgebirge"
#### OberesRheintal
OR <- list(c(RP_OR, BW_OR, HS_OR))
names(OR) <- "OberesRheintal"
#####  Bodensee Schwarzwald
BS <- list(BW_BS)
names(BS) <- "BodenseeSchwarzwald"
#### Norddeutsche Küstenregion
NK <- list(c(MV_NK, SH_NK, LS_NK))
names(NK) <- "NorddeutscheKüstenregion"
#### Nord Osten MV
NOMV <- list(c(MV_NOMV, BB_NOMV))
names(NOMV) <- "NordOstenMV"
#### Leichte Böden Nordwest 
LBN <- list(c(LS_LBN, NRW_LBN))
names(LBN) <- "LeichteBödenNordwest"
#### Hannover, Hildesheim
HH <- list(LS_HH )
names(HH) <- "HannoverHildesheim"
#### Nordosten Brandenburg
NOBB <-  list(c(BB_NOBB, MV_NOBB, SN_NOBB, SA_NOBB))
names(NOBB) <- "NordostenBrandenburg"
#### Mitte Osten
MO <- list(c(SN_MO, SA_MO, TH_MO))
names(MO) <- "MitteOsten"
#### Rhein-Hessiche Ackerbaugebiete 
RHA <- list(c(NRW_RHA, HS_RHA))
names(RHA) <- "RheinHessicheAckerbaugebiete"
#### Harz Rhön Th. Wald Erzgebirge By.Wald
MG <- list(c(SN_MG, SA_MG, TH_MG, LS_MG))
names(MG) <- "HarzRhönThWaldErzgebirgeByWald"
#### Westliches Mittelgebirge
WMG <- list(c(LS_WMG, NRW_WMG, HS_WMG))
names(WMG) <- "WestlichesMittelgebirge"
#### Übergangslagen
UL <- list(c(LS_UL, HS_UL, BY_UL, BW_UL, TH_UL, SN_UL))
names(UL) <- "Ubergangslagen"
####  Ost Bayern
OB <- list(BY_OB)
names(OB) <- "OstBayern"
#### Donau Inntal
DI <- list(c(BW_DI, BY_DI))
names(DI) <- "DonauInntal"
#### Bodensee Schwarzwald Schw. Alb
BS <- list(BW_BS)
names(BS) <- "BodenseeSchwarzwaldSchwAlb"
#### Voralpen Alpen
VA <- list(BY_VA)
names(VA) <- "VoralpenAlpen"

list_BKR <-c(SWMG, OR, BS, NK, NOMV, LBN, HH, NOBB, MO,RHA, MG, WMG, UL, OB, DI, VA)
list_BKR[10]

#### Combine FS with no Mittelgebirge (MG, SWMG, WMG, BS) und keine kreisfreien Städte
list_FSnoMGKS <- list("SH_noMGKS" = SH_noMGKS, "LS_noMGKS" = LS_noMGKS, "NRW_noMGKS" = NRW_noMGKS, "HS_noMGKS" = HS_noMGKS, "RP_noMGKS" = RP_noMGKS, "BW_noMGKS" = BW_noMGKS,
                      "BY_noMGKS" = BY_noMGKS, "BB_noMGKS" = BB_noMGKS, "MV_noMGKS" = MV[[1]], "SN_noMGKS" = SN_noMGKS, "SA_noMGKS" = SA_noMGKS, "TH_noMGKS" = TH_noMGKS) 
'MV gibt es per se keine Mittelgebirge und Kreisfreien Städte. SL is takken out as it is entirely comprised of MG'

#### Combine all spatial subsets into one
list_spatial_sp <- list("Manual_cluster"= list("BodenKlimaRäume"= list_BKR, "FederalStates" = list_FS,  "FederalStatesNoKs" = list_FS_noks, 
                                               "FederalStatesNoMGNoKs" = list_FSnoMGKS))
'BGR always come without KS'
str(list_spatial_sp,2)
names(list_spatial_sp)

# ###########################################################
# #### Combine with cluster gained in Clustering Script ####
# #########################################################
# ## Load cluster from 
# 
# # list_spatial <- append(list_spatial_cluster, list_spatial_sp)
# save(list_spatial_sp, file = paste(directory_results, "list_spatial.RData", sep=""))
# # list_spatial_sp.df <- do.call("rbind", lapply(list_spatial_sp, as.data.frame)) 
# 
# list_spatial_sp_2 <- load(paste(directory_results, "list_spatial.RData", sep=""))
# list_spatial_sp_2
