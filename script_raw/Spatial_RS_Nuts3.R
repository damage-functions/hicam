################################################################################################################################################
#### Create shape file for the administrative districts (kreise)  based on NUTS3 (401 units) that contain information for both NUTS3 and RS ####
#### (comId so in the scripts before) - the reason is that for the HICAM project WGS84 projetions with lotlan based on the NUTS3 nomenclatur####
#### is used. However, the yield data in the Regionalstatistik are based on RS (Amtlicher Regionalschlüssel (ARS)).                         ####
################################################################################################################################################

#### Input #### 
# - Verwaltungszuordnung1 : 250000VZ250 (http://sg.geodatenzentrum.de/web_download/vz/vz250_0101/vz250_0101.pdf - 2018)
#   VZ250_GEM <- data_spatial/VZ250_GEM.csv
# - Spatial Data used in HICAM (for Europe) - stored in data/Hicam on cluster:
#   kreise <- directory_spatial, "nuts3/NUTS_RG_01M_2016_4326_LEVL_3.shp"

#### Output ####
# - Shape file based on NUTS3 (401 units) also containing referencing to RD
#   directory_spatial, "NUTS3_RS/Kreise_Nuts3_RS.shp"
# - SpaceTime_Nuts3 contain both spatial information for NUTS3 and RS for period 1951 - 2020
#   directory_spatial, "spaceTime_NUTS3.csv"

 
###############################
#### Load libraries needed ####
library("sf")
library("tidyverse")

#####################################################################################
#### Geospatial information that contain both information for RS (ARS) and NUTS3 ####

#### Load File ####
VZ250_GEM <- read_csv("../data/data_spatial/VZ250_GEM.csv")

#### Select relevant variables ####
VZ250_GEM <- VZ250_GEM %>% select(`ARS_K,C,12`, "GEN_K,C,60", "NUTS3_CODE,C,5", "NUTS3_NAME,C,100", 
                            "NUTS2_CODE,C,5", "NUTS2_NAME,C,100", "NUTS1_CODE,C,5", "NUTS1_NAME,C,100") %>% unique()

#### Change names ####
names(VZ250_GEM) <- c("RS", "RS_name", "NUTS3", "NUTS3_name","NUTS2", "NUTS2_name","NUTS1", "NUTS1_name" )

str(VZ250_GEM)

#### Export new data.frame ####
write_csv(VZ250_GEM, "../data/data_spatial/RS_Nuts3.csv")


###################################################################################
#### Spatial Data used in HICAM (for Europe) - stored in data/Hicam on cluster ####

####  Load Data ####
kreise <- read_sf(paste(directory_spatial, "nuts3/NUTS_RG_01M_2016_4326_LEVL_3.shp", sep=""))
str(kreise,2) #1522 

#### Filter for Cntr_Code Germany ####
kreise_D <- kreise %>% filter(CNTR_CODE == "DE")
str(kreise_D) # 401 Ids

#### Plot shape ####
# plot(kreise_D)

#### Check Projection ####
st_crs(kreise_D) # WGS84
st_geometry(kreise_D)

#### Check order and names of the Kreise in the shape ####
kreise_D %>% select(NUTS_ID, NUTS_NAME) ## The order of the LK is the same as those  found for the netcdf 


#### Join the kreise_shape with the spatial information containing both RS and NUTS information ####
nuts_leftjoin <- left_join(kreise_D, VZ250_GEM, by =c("NUTS_ID" = "NUTS3"))
nuts_leftjoin

#### Select only relevant data ####
nuts_leftjoin <- nuts_leftjoin %>% select(LEVL_CODE:RS)
nuts_leftjoin

#### Plot Shape ####
# nuts_leftjoin %>% plot()

#### Write the shape file based on NUTS3 (401 units) also containing referencing to RS ####
write_sf(nuts_leftjoin , paste(directory_spatial, "NUTS3_RS/Kreise_Nuts3_RS.shp", sep=""), driver = "ESRI Shapefile") 

###########################################################################################################
#### Generate spaceTime_Nuts3 contain both spatial information for NUTS3 and RS for period 1951 - 2020 ####

## Repeat each year 401 times ##
start_year = 1951
end_year = 2020
year_diff = end_year - start_year + 1

years <- sort(rep(seq(start_year, end_year), 401))
years %>% unique()


## Repeat each NUTS_ID and RS year_diff times
NUTS3 <- rep(nuts_leftjoin$NUTS_ID, year_diff)
NUTS3
RS <- rep (nuts_leftjoin$RS, year_diff)

#### COmbine years, NUTS3, and RS ####
spaceTime_NUTS3 <- bind_cols("years" = years, "NUTS3" = NUTS3, "RS" = RS)

#### Export data_frame ####
write_csv(spaceTime_NUTS3, paste(directory_spatial, "spaceTime_NUTS3.csv", sep=""))
