# --------------------------------------------------
#  File:        execute_script.sh
#
#  Created:     Mi 10-02-2021
#  Author:      Friedrich Boeing
#
# --------------------------------------------------
#
#  Description: this scripts applies the weighting program to calculate nuts-3 averages and loops over variables
#
#  Modified:
#
# --------------------------------------------------

 
set -e
set -x
do_cluster="False"
mainpath="/data/klimabuero/agri_hicam/"
progpath="${mainpath}/progs/"
source ${mainpath}/progs/weights/moduleLoadScripts/eve.intel18
mask="${mainpath}/input/mask_landuse/mask_agri_hicam_de_final.nc"
inpath="/data/hicam/data/processed/meteo/germany/station_eobs/mhm/"
# var="pre"
# change to preprocessing soilmoisture before weighting:
vars=("pre" "tavg" "tmax" "tmin" "pet")
# vars=("pre")
# vars=("tavg" "tmax" "tmin")
outpath="meteo_hist"
if [ ! -d $outpath ]; then
   mkdir $outpath
fi
cd $outpath

for var in "${vars[@]}"; do
if [[ $do_cluster == "True" ]]; then


cat > submit_${var}.sh <<EOF
#!/bin/bash

#SBATCH -J agri_hicam_${var}
#SBATCH -o ./%x-%j.log
#SBATCH -t 0-48:00:00
#SBATCH --mem-per-cpu=48G
#SBATCH --mail-user=friedrich.boeing@ufz.de
#SBATCH --mail-type=BEGIN,END
#SBATCH --output=./%x-%A-%a.out
#SBATCH --error=./%x-%A-%a.err

source ${mainpath}/progs/weights/moduleLoadScripts/eve.intel18
bash ${progpath}/setup_weights.sh $var ${inpath}/${var}.nc $mask
ln -fs $progpath/weights/weights_time_sp .
time ./weights_time_sp
EOF

sbatch submit_${var}.sh
else


    bash ${progpath}/setup_weights.sh $var $inpath/$var.nc $mask
    ln -fs $progpath/weights/weights_time_sp .
    time ./weights_time_sp &> logfile_${var}.txt
fi
done
