#!/bin/bash

#SBATCH -J testR
#SBATCH -o ./jobs/%x-%j.log
#SBATCH -e ./jobs/%x-%j.err
#SBATCH -t 0-02:02:00

#SBATCH --mem-per-cpu=24G
##SBATCH -c 12

#SBATCH -N 1
#SBATCH --ntasks-per-node=12

#SBATCH --mail-user=michael.peichl@ufz.de
#SBATCH --mail-type=BEGIN,END
#SBATCH --output=./%x-%A-%a.out
#SBATCH --error=./%x-%A-%a.err

## Load required modules

# ml foss/2019b
# ml NCO
# ml CDO

ml Anaconda3

## Set the requested number of cores to the number of Threads the app should use
export OMP_NUM_THREADS=${SLURM_CPUS_PER_TASK:-1}
export MC_CORES=${SLURM_CPUS_PER_TASK_-1}

source activate r-env

Rscript ./script_raw/Spatial_netcdf_to_sf.R


# ncap2 -O -s 'time=int(time)' tavg.nc tavg2.nc
# ncap2 -O -s 'time=int(time)' tmax.nc tmax2.nc
# ncap2 -O -s 'time=int(time)' tmin.nc tmin2.nc

